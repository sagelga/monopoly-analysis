![](img/monopoly-logo.png)
# Monopoly Analysis
*version Alpha 0.1*

The famous fast-trading board game Monopoly meets decision support analysis.

> Monopoly, the fast-dealing property trading game where players buy, sell, dream and scheme their way to riches

**WARNING** This program **currently** have no user interface, as we released the Alpha 0.1.<br>
User that cannot troubleshoot Python code should not try the program until beta.

## ![](img/icons8-goal-24.png) Objective
This repository and project is created for **decision support** usage.<br>
Users can use this service in decisions-making purpose that could make the game more exciting.

The analysis will be based on information given, statistics calculations and many risks factor that other player have create.

Users that use this program will able to:
- Understand the risks of making a move
- Estimates the property values and upgrades
- Show risks and rewards throughout the gameplay

and of course, understand what is going to happen next, just like you do. Win.

## ![](img/icons8-features-list-24.png) Features
Play Monopoly in your way
- Edit the game rule to fit your game experiences
- Make your game more random with Speed Dice support
- Buy or sell a property and home with the interface

Also, this service can mame a suggestions on current game status
- Show property estimated values
- Show rent on a property valuations
- Probability of getting a bankrupt
- Probability to land on the desired space

Create a report on the game status and after-game

and many more!

## ![](img/icons8-downloading-updates-24.png) Installation
Download this repository by cloning or click [here](https://github.com/sagelga/monopoly-analysis/archive/master.zip) to download the .zip file.

> This program requires Python programming language and SQLite. Please download prior the launch of the program.

If you do not have Python or/and SQLite, you can run (for Linux)
```
sudo apt-get install python3
sudo apt-get install sqlite
```
and many other ways to install `python3` and `sqlite3`.<br>

> NOTE : Library dependencies list will listed here, in README.md file.

## ![](img/icons8-exe-24.png) How to start
You can checkout the `configuration.py` for edits on game settings, including starting money, jail penalty, etc.

If you have open the folder in Terminal, you can start by running this prompt
```
python3 main.py
```

Python supports Windows, MacOS, Linux Distributions. You can start the game from Command Prompt (Windows), Windows PowerShell (Windows) and Terminal (MacOS and Linux Distribution)

## ![](img/icons8-game-controller-24.png) How to use
> For more details, please check out our [User Manual](https://sagelga.github.io/monopoly-analysis/).

We recommend you to play the Monopoly game on a real Monopoly board game <br>
and use this service to provide more information during the real-life gameplay. Our project goal is to support.

There is a few steps of the game that you need to follow. You will be asked for multiple inputs (eg. turns, decision made, dice rolls result) and there will be prompt in the command line (for current version)

Step 1: Type in **all** of the users.<br>
Step 2: Type in the orders that each user get (or skip if you liked to make us random)<br>
Step 3: Type in the rolls you get (or skip if you liked to make us random)<br>
Step 4: Choose to do any actions (Buy/Trade Property, Auction, Make/Redo Mortgage, Buy/Sell Assets or Finish Turn)<br>

and the process will repeat until we have the last man standing

The interface will show the current game status. You

## ![](img/icons8-user-manual-24.png) Development Documentation
The documentation on how to use/modify our modules, projects is available [here : https://sagelga.github.io/monopoly-analysis/](https://sagelga.github.io/monopoly-analysis/)


### Module distribution
This project is divided to 6 modules (Python file), which is<br>
[`main.py`](https://github.com/sagelga/monopoly-analysis/blob/master/main.py)
[`service.py`](https://github.com/sagelga/monopoly-analysis/blob/master/service.py)
[`actions.py`](https://github.com/sagelga/monopoly-analysis/blob/master/actions.py)
[`transaction.py`](https://github.com/sagelga/monopoly-analysis/blob/master/transaction.py)
[`database.py`](https://github.com/sagelga/monopoly-analysis/blob/master/database.py)
[`configuration.py`](https://github.com/sagelga/monopoly-analysis/blob/master/configuration.py)

which each module has it's own purpose
- main          : serves as program initiator + flow control
- service       : handle action made by user or asked for response from user
- action        : handle background action
- transaction   : prepare transaction to be made on database
- database      : create transaction directly to SQLite database
- configuration : user defined rules, which can **only** be retrieved.

User can edit the configuration to suit their game.

## ![](img/icons8-code-fork-24.png) Contribute
We welcome all of the developers into the development of this program.<br>
We are widely open for the pull requests from all developers to improve this project.<br>
Your contributions will make this project thrive.

For the new comers, please do as follows
1. Fork this repository
2. Create a new branch at the forked repository and named it as `your_username` or `issueID`
3. Create a pull request in this repository [here](https://github.com/sagelga/monopoly-analysis/pulls)
4. We will make a suggestions and merged your changes

## Other Stuff
### Edit your own game
Please check on the [documentation page](https://github.com/sagelga/monopoly-analysis/wiki/Configurations) on how you can edit the game settings

> NOTE : You should able to read the Python syntax to make a correct change for the game.

### Project Checker
**Fossa**

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fsagelga%2Fmonopoly-analysis.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Fsagelga%2Fmonopoly-analysis?ref=badge_large)

**Codacy**

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/600b857dd7eb47198768a3031efdb888)](https://www.codacy.com/app/sagelga/monopoly-analysis?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=sagelga/monopoly-analysis&amp;utm_campaign=Badge_Grade)

**CircleCI**

|master|develop|
|------|-------|
|[![CircleCI](https://circleci.com/gh/sagelga/monopoly-analysis/tree/master.svg?style=svg)](https://circleci.com/gh/sagelga/monopoly-analysis/tree/master)|[![CircleCI](https://circleci.com/gh/sagelga/monopoly-analysis/tree/develop.svg?style=svg)](https://circleci.com/gh/sagelga/monopoly-analysis/tree/develop)|

### Boring things that you need to know
As you are reading or using or derived or forked or copying code from this project, you have accepted the Terms and Conditions and our Apache Licenese.<br>
Please make sure that you are following our legal terms.

> Please check out the [LICENSE.md](https://github.com/sagelga/monopoly-analysis/blob/master/LICENSE.md) for more information about our Apache Open Source license.

#### Simplified Terms and Conditions
The project is fan-made and built in the purpose of **support** the players (user) into making the decisions along the game play.

This project can be used in non-commercial or/and non-profit purpose and requires the users to respect all the developers, copyright owner(s) and trademark owner(s). 
We are not response of any liability from using our service or repository or source code. We also NOT supporting you to use this repository to create hate, conflicts or anything that makes other feel bad. A forked branch is over our control, thus you are on your own.<br>

If you liked to use this repository in a commercial or for-profit benefits, please directly contact me and read the full agreements in LICENSE.md file

If you liked to suggests on legal terms we have in this repository, do not afraid to contact me via email, provided on my GitHub profile page.

Information about Monopoly game rule is from official Monopoly website and [Wikipedia page](https://en.wikipedia.org/wiki/Monopoly_(game))

Icons inside this project's README file is from [Icons8](icons8.com).

---

[![](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)
[![](https://forthebadge.com/images/badges/contains-cat-gifs.svg)](https://forthebadge.com)
[![](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)
[![](https://forthebadge.com/images/badges/powered-by-netflix.svg)](https://forthebadge.com)
